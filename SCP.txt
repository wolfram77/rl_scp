Simple Communication Protocol
-----------------------------

Network Layer
-------------
[Size: 8][Checksum: 8][Dest: 16][Source: 16]
Overhead: 6 bytes

Notes:
Size >= 6
Checksum == XOR + ROR Checksum (afterwards)
Source  > 0  (1 => root node / gateway)
Same rules apply to Dest


Transport Layer
---------------
[ID: 8][Sequence: 8][Flags: 8]
Overhead: 3 bytes

Notes:
ID > 0, increment
Sequence (0 for full packet, > 0 for partial packet)
Flags [b7|b6|b5|b4| b3 Nack Reply | b2 Require Nack | b1 Ack Reply | b0 Require Ack]
In case of Ack / Nack Reply Sequence == 0 => full packet ack / nack
Source + ID + Sequence can be used as a 32-bit unique ID for packet.


Network Layer
-------------
[Size: 16][Checksum: 16][Dest: 64][Source: 64]
Overhead: 20 bytes

Transport Layer
---------------
[ID: 16][Sequence: 16][Flags: 16]
Overhead: 6 bytes

Notes:
(Computer use this directly upon UDP/TCP)
Size >= 24
Checksum == XOR + ROR Checksum (afterwards)
LowDword(Source) == 0 => Internet router
LowDword(Source) == 0 && LowWord(HighDword(Source)) == 0 => Internet router manager
Same rules apply to Dest
Checksum + ID can be used as a 32-bit unique ID for packet.


Reduced Instruction Set
-----------------------
Regsiters:
0 = AL (data)
1 = AH (data)
2 = BL (data)
3 = BH (data)
4 = CL (data)
5 = CH (data)
6 = DL (data)
7 = DH (data)
8 = PL (ptr, def, proc)
9 = PH (ptr, def, proc)
A = QL (ptr, query)
B = QH (ptr, query)
C = RL (ptr, return)
D = RH (ptr, return)
E = Flags LSB
F = Flags MSB

Instructions:
00 00	= Nop
00 01	= return R<C>
00 02	= call R<C> = *P<A>(Q<B>)

01 0b	= mov *P, b
01 1b	= mov b, *P
01 2b	= shl b, CL
01 3b	= shr b, CL
sar	b, CL
ror	b, CL
rol	b, CL
rcr	b, CL
rcl b, CL
not	b
neg	b
inc b
dec b


Favv: mov aaaa, vvvvvvvv
E0ab: mov aaaa, bbbb
E10b: mov *?, bbbb
E11b: mov bbbb, *?

return 

E1ab: mov *aaaa, bbbb
E0ab: adc aaaa, bbbb
E1ab: sbb aaaa, bbbb
E2ab: and aaaa, bbbb
E3ab: or  aaaa, bbbb
E4ab: xor aaaa, bbbb

mul aaaa
div aaaa
shl aaaa
shr aaaa
jmp rel
E0ab: mov aaaa, bbbb

4a0b: subtract
call inst return
0002aavv	= Set memory

